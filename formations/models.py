from django.contrib.auth.models import User
from django.db import models


class FiliereTag(models.Model):

    tag = models.CharField(max_length=64)


class Filiere(models.Model):

    nom = models.CharField(max_length=256)

    url = models.CharField(max_length=256)

    tags = models.ManyToManyField(FiliereTag)

    sim = models.ManyToManyField('self')


class Etablissement(models.Model):

    nom = models.CharField(max_length=256)

    latitude = models.FloatField()

    longitude = models.FloatField()


class FormationType(models.Model):

    type = models.CharField(max_length=16)

    nom = models.CharField(max_length=256)


class FormationTag(models.Model):

    tag = models.CharField(max_length=64)


class Formation(models.Model):

    nom = models.CharField(max_length=256)

    etablissement = models.ForeignKey('Etablissement', on_delete=models.CASCADE)

    type = models.ForeignKey('FormationType', on_delete=models.CASCADE)

    filiere = models.ForeignKey('Filiere', on_delete=models.CASCADE)

    places = models.IntegerField()

    bac_general = models.IntegerField()
    bac_technologique = models.IntegerField()
    bac_professionnel = models.IntegerField()

    tags = models.ManyToManyField('FormationTag')

    ind = models.IntegerField()


class Formateur(models.Model):

    user = models.OneToOneField(User)

    etablissement = models.ManyToManyField('Formation')
