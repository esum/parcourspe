
Pour générer un élève :
  1. Lui choisir un nom au hasard (fichier : nom.txt)
  2. Lui choisir un prenom au hasard (fichier : prenom.txt)
  3. Lui choisir un lycée au hasard (fichier : lycee.txt)
  4. Lui choisir un bac au hasard (fichier : bac.txt)
  5. Lui choisir une date de naissance, un sexe et un ine (RNG)
  6. Lui affecter des notes
    a. Avoir la liste des matières liées a son bac 
    b. Pour chacune des matières tirer une note entre 0 et 20 (distribution normale)
  7. Lui choisir une liste de voeux 
    a. Depuis la liste des formations qui lui sont accessibles (bac), en choisir entre 0 et 10

