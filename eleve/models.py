from django.contrib.auth.models import User
from django.db import models


class Bac(models.Model):

    bac = models.CharField(max_length=256)

    type = models.CharField(max_length=1)


class Academie(models.Model):

    nom = models.CharField(max_length=256)


class Departement(models.Model):

    nom = models.CharField(max_length=256)

    code = models.CharField(max_length=8)

    academie = models.ForeignKey('Academie', on_delete=models.CASCADE)


class Commune(models.Model):

    nom = models.CharField(max_length=256)

    code = models.CharField(max_length=8)

    departement = models.ForeignKey('Departement', on_delete=models.CASCADE)


class Lycee(models.Model):

    nom = models.CharField(max_length=256)

    commune = models.ForeignKey('Commune', on_delete=models.CASCADE)

    code = models.CharField(max_length=16)

    public = models.BooleanField()

    bacs = models.ManyToManyField('Bac')


class Eleve(models.Model):

    user = models.OneToOneField(User)

    nom = models.CharField(max_length=256)
    prenom = models.CharField(max_length=256)
    sexe = models.NullBooleanField(blank=True)
    ine = models.CharField(max_length=16)
    date_de_naissance = models.DateField()

    mail = models.EmailField()

    lycee = models.ForeignKey('Lycee', on_delete=models.CASCADE)

    bac = models.ForeignKey('Bac', on_delete=models.CASCADE)


class Voeu(models.Model):

    eleve = models.ForeignKey('Eleve', on_delete=models.CASCADE)

    formation = models.ForeignKey('formations.Formation', on_delete=models.CASCADE)

    classement = models.IntegerField()

    lettre_motivation = models.TextField()

    class Meta:
        unique_together = (
            ('eleve', 'formation'),
            ('eleve', 'classement')
        )


class Matiere(models.Model):

    nom = models.CharField(max_length=256)


class Note(models.Model):

    eleve = models.ForeignKey('Eleve', on_delete=models.CASCADE)

    matiere = models.ForeignKey('Matiere', on_delete=models.CASCADE)

    note = models.FloatField()
    note_max = models.FloatField()

    remarque = models.TextField()

    class Meta:
        unique_together = ('eleve', 'matiere')


class Prof(models.Model):

    user = models.OneToOneField(User)

    lycee = models.ForeignKey('Lycee', on_delete=models.CASCADE)

    matieres = models.ManyToManyField('Matiere')

    eleves = models.ManyToManyField('Eleve')
