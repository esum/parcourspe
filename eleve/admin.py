from django.contrib import admin

from .models import Bac, Academie, Departement, Commune, Lycee, Eleve, Voeu, Matiere, Note, Prof

@admin.register(Bac)
class BacAdmin(admin.ModelAdmin):
    list_display = ('bac', 'type')

@admin.register(Academie)
class AcademieAdmin(admin.ModelAdmin):
    list_display = ('nom',)

@admin.register(Departement)
class DepartementAdmin(admin.ModelAdmin):
    list_display = ('nom', 'code', 'academie')

@admin.register(Commune)
class CommuneAdmin(admin.ModelAdmin):
    list_display = ('nom', 'code', 'departement')

@admin.register(Lycee)
class LyceeAdmin(admin.ModelAdmin):
    list_display = ('nom', 'commune', 'code')

@admin.register(Eleve)
class EleveAdmin(admin.ModelAdmin):
    list_display = ('nom', 'prenom', 'ine')

@admin.register(Voeu)
class VoeuAdmin(admin.ModelAdmin):
    list_display = ('eleve', 'formation', 'classement')

@admin.register(Matiere)
class MatiereAdmin(admin.ModelAdmin):
    list_display = ('nom', )

@admin.register(Note)
class NoteAdmin(admin.ModelAdmin):
    list_display = ('eleve', 'matiere', 'note')

@admin.register(Prof)
class ProfAdmin(admin.ModelAdmin):
    list_display = ('user', 'lycee')
